#include <fstream>
#include <iostream>

#include "capnp/list.h"
#include "capnp/message.h"
#include "capnp/serialize-packed.h"
#include "kj/std/iostream.h"

#include "maidsafe/common/person.capnp.h"

int main() {
  capnp::MallocMessageBuilder message;
  maidsafe::common::Person::Builder person = message.initRoot<maidsafe::common::Person>();
  person.setName("Foo Bar Baz");
  person.setEmail("foo@bar.baz");
  maidsafe::common::Date::Builder date = person.initBirthdate();
  date.setDay(31);
  date.setMonth(3);
  date.setYear(1973);

  capnp::List<maidsafe::common::Person::PhoneNumber>::Builder phones = person.initPhones(2);
  phones[0].setNumber("(123) 123-1234");
  phones[0].setType(maidsafe::common::Person::PhoneNumber::Type::HOME);
  phones[1].setNumber("(123) 123-1235");
  phones[1].setType(maidsafe::common::Person::PhoneNumber::Type::MOBILE);

  capnp::writePackedMessage(
      kj::std::StdOutputStream(std::ofstream("foo.txt", std::ios::binary | std::ios::trunc)),
      message);

  return 0;
}
